package dk.digitalidentity.tokenfetcher;

import dk.digitalidentity.tokenfetcher.sts.ClaimsCallbackHandler;
import dk.digitalidentity.tokenfetcher.sts.STSClient;
import org.apache.cxf.Bus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource(value = "file:client.properties")
@ImportResource({ "classpath:META-INF/cxf/cxf.xml" })
public class CXFConfiguration {

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${org.apache.ws.security.crypto.merlin.keystore.alias}")
    private String keystoreAlias;

    @Bean
    public ClaimsCallbackHandler claimsCallbackHandler(){
        return new ClaimsCallbackHandler();
    }

    @Bean
    public STSClient kombitSTSClient(){
        Bus bus = (Bus) applicationContext.getBean(Bus.DEFAULT_BUS_ID);

        STSClient stsClient = new STSClient(bus);
        stsClient.setWsdlLocation("classpath:sts.wsdl");
        stsClient.setServiceName("{http://schemas.microsoft.com/ws/2008/06/identity/securitytokenservice}SecurityTokenService");
        stsClient.setEndpointName("{http://schemas.microsoft.com/ws/2008/06/identity/securitytokenservice}CertificateWSTrustBinding_IWSTrust13Sync");
        stsClient.setSendRenewing(false);
        stsClient.setKeyType("http://docs.oasis-open.org/ws-sx/ws-trust/200512/PublicKey");
        stsClient.setSendKeyType(true);
        stsClient.setTokenType("http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0");
        stsClient.setRequiresEntropy(false);
        stsClient.setClaimsCallbackHandler(claimsCallbackHandler());

        Map<String, Object> properties = new HashMap<>();
        properties.put("ws-security.is-bsp-compliant", "false");
        properties.put("ws-security.callback-handler", "dk.digitalidentity.tokenfetcher.sts.PasswordCallbackHandler");
        properties.put("ws-security.add.inclusive.prefixes", "false");
        properties.put("ws-security.signature.username", keystoreAlias);
        properties.put("ws-security.signature.properties","client.properties");
        properties.put("ws-security.encryption.username", "sts");
        properties.put("ws-security.encryption.properties", "sts.properties");
        properties.put("ws-security.sts.token.properties", "client.properties");
        properties.put("ws-security.sts.token.username", keystoreAlias);
        properties.put("ws-security.asymmetric.signature.algorithm", "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");

        stsClient.setProperties(properties);

        return stsClient;
    }
}
