package dk.digitalidentity.tokenfetcher.sts;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.ws.security.trust.claims.ClaimsCallback;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClaimsCallbackHandler implements CallbackHandler {
	private static String cvr; // cvr is static so that the external bean 'CommandLineArguments' is able to set alter it.

	@Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof ClaimsCallback) {
				ClaimsCallback callback = (ClaimsCallback) callbacks[i];
				callback.setClaims(createClaims());
			}
			else {
				throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
			}
		}
	}

	private static Element createClaims() {
		Document doc = DOMUtils.createDocument();
		Element claimsElement = doc.createElementNS("http://docs.oasis-open.org/ws-sx/ws-trust/200512", "Claims");
		claimsElement.setAttributeNS(null, "Dialect", "http://docs.oasis-open.org/wsfed/authorization/200706/authclaims");

		Element claimType = doc.createElementNS("http://docs.oasis-open.org/wsfed/authorization/200706", "ClaimType");
		claimType.setAttributeNS(null, "Uri", "dk:gov:saml:attribute:CvrNumberIdentifier");
		claimsElement.appendChild(claimType);

		Element claimValue = doc.createElementNS("http://docs.oasis-open.org/wsfed/authorization/200706", "Value");
		claimValue.setTextContent(cvr);
		claimType.appendChild(claimValue);

		return claimsElement;
	}

	public static String getCvr() {
		return cvr;
	}

	public static void setCvr(String cvr) {
		ClaimsCallbackHandler.cvr = cvr;
	}
}
