package dk.digitalidentity.tokenfetcher;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.cxf.ws.security.tokenstore.SecurityToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;

import dk.digitalidentity.tokenfetcher.sts.STSClient;

@Component
public class TokenFetcherRunner implements CommandLineRunner {

	@Autowired
	private STSClient stsClient;

	@Autowired
	private CommandLineArguments commandLineArguments;

	@Override
	public void run(String... strings) throws Exception {
		SecurityToken request = stsClient.requestSecurityToken(commandLineArguments.getEntityId());

		OutputStream outputStream = new FileOutputStream(commandLineArguments.getPath());
		writeElementToStream(request.getToken(), outputStream);
	}

	private static void writeElementToStream(Element node, OutputStream outputStream) throws Exception {
		Source source = new DOMSource(node);
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		StringWriter buffer = new StringWriter();

		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.transform(source, new StreamResult(buffer));

		outputStream.write(buffer.toString().getBytes(Charset.forName("UTF-8")));
	}
}
