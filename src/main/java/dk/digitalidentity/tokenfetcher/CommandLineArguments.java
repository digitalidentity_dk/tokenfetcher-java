package dk.digitalidentity.tokenfetcher;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import dk.digitalidentity.tokenfetcher.sts.ClaimsCallbackHandler;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class CommandLineArguments {
	private String path;
	private String cvr;
	private String entityId;

	@Autowired
	private Environment env;

	@PostConstruct
	public void init() {
		if (env.getProperty("nonOptionArgs") == null) {
			throw new BeanCreationException("Missing parameters. Syntax : tokenfecher <cvr> <entity-id> <output-file-path>");
		}
		
		String[] nonOptionArgs = env.getProperty("nonOptionArgs").split(",");
		if (nonOptionArgs.length != 3) {
			throw new BeanCreationException("Invalid parameters. Syntax : tokenfetcher <cvr> <entity-id> <output-file-path>");
		}

		cvr = nonOptionArgs[0];
		entityId = nonOptionArgs[1];
		path = nonOptionArgs[2];

		ClaimsCallbackHandler.setCvr(cvr);
	}
}